# VuePress 在 [21云盒子](https://www.21cloudbox.com/)的样例

这是 [21云盒子](http://www.21cloudbox.com/) 上创建的 [VuePress](https://vuepress.vuejs.org/) 样例。

## 部署

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-vuepress-project-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-vuepress-project-in-production-server.html)
